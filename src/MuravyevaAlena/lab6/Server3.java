
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

class ClientJob{
   String jobName;
   String time;
   String clientIp;
   Integer clientPort;
   
   boolean isActive = true;
   
   public ClientJob(String aClientIp, int aClientPort, String aTime, String aJob){
      clientIp = aClientIp;
      clientPort = aClientPort;
      jobName=aJob;
      time=aTime;
   }
}

public class Server3 implements Runnable {
   private Socket connection;
   private ObjectOutputStream output;
  
   GregorianCalendar calendar= new GregorianCalendar();
   String localIpAdress=new String();
   int localPort=0;
   List<ClientJob> jobList=new ArrayList<ClientJob>();

   int today;
   int month;
   int year;
   int hour;
   int minutes;
   int second;
   
   @Override
   public void run() {
      readFile();
      try {
         while(true){
            calendar.setTime(new Date());
            hour=calendar.get(Calendar.HOUR);
            minutes=calendar.get(Calendar.MINUTE);
            String currentTime= hour + ":" + minutes;
            
            for(ClientJob job : jobList)
            {
               if(currentTime.compareTo(job.time) == 0 && job.isActive){
                  connection= new Socket(job.clientIp, job.clientPort);
                  output=new ObjectOutputStream(connection.getOutputStream());
                  System.out.println("Server: Send job ("+job.jobName+") to "+ job.clientIp + ":" + job.clientPort);
                  output.writeObject(job.jobName);
                  output.flush();
                  connection.close();
                  output.close();
                  job.isActive = false;
               }
            }
            System.out.println("Server: Check time:" + currentTime);
            Thread.sleep(5000);
         }
      }
      catch (HeadlessException e){
         e.printStackTrace();
      } 
      catch (UnknownHostException e){
         e.printStackTrace();
      } 
      catch (IOException e){
         e.printStackTrace();
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
   }
   
   public void readFile(){
      File jobs=new File("spisokDel.txt");
      BufferedReader breader = null;
      try {
         breader = new BufferedReader(
               new InputStreamReader(
               new FileInputStream (jobs),"UTF8"));
      } catch (UnsupportedEncodingException e1) {
         e1.printStackTrace();
      } catch (FileNotFoundException e1) {
         e1.printStackTrace();
      }
      
      String nextLine;
      String [] splitedLine;
      try {
         while( (nextLine=breader.readLine()) != null){
            splitedLine=nextLine.split(",");
            Integer port = new Integer(splitedLine[1]);
            jobList.add(
                  new ClientJob(
                        splitedLine[0], 
                        port, 
                        splitedLine[2], 
                        splitedLine[3]));
          }
      } catch (IOException e) {
         e.printStackTrace();
      }
    }
}
