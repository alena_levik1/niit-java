
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;

public class Client3 implements Runnable{
   
   private Socket connection;
   private ObjectInputStream input;
   private ServerSocket acceptSocket;
   
   String ipAdress;
   int numberPort;
   
   public Client3(int aNumberPort){
      numberPort=aNumberPort;
   }

   @Override
   public void run() {
      try {
         acceptSocket=new ServerSocket(numberPort,10);
            
         while(true){
            connection=acceptSocket.accept();
            input=new ObjectInputStream(connection.getInputStream());
          
            String job = (String)input.readObject();
            System.out.println("Client: Accepted new job: "+job);
            connection.close();
            input.close();
         }
      }
      catch (HeadlessException e){
         e.printStackTrace();
      } 
      catch (ClassNotFoundException e){
         e.printStackTrace();
      }
       catch (UnknownHostException e){
         e.printStackTrace();
      } catch (IOException e){
         e.printStackTrace();
      }
   }
}

