
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;

public class ClientClock extends JFrame implements Runnable{
   static private Socket connection;
   static private ObjectOutputStream output;
   static private ObjectInputStream input;
   JTextField timeText =new JTextField(15);
   
   public static void main(String[] args) {
      new Thread (new ClientClock ()).start();
      new Thread (new ServerClock()).start();
   }
   public ClientClock(){
      setTitle("Client-Server Clock");
      setLayout(new FlowLayout());
      setSize(500,300);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setLocationRelativeTo(null);
    
      JButton timeButton= new JButton("Show time");
      add(timeButton);
      
      timeText.setEditable(false);
      add(timeText);
      setResizable(false);
      setVisible(true);
      timeButton.addActionListener(new ActionListener(){
         @Override
         public void actionPerformed(ActionEvent arg0){
            sendDate("Show time");
         }
      });
   }

   @Override
   public void run() {
      try {
         while(true){
            connection= new Socket(InetAddress.getByName("127.0.0.1"),2503);
            output=new ObjectOutputStream(connection.getOutputStream());
            input=new ObjectInputStream(connection.getInputStream());
            timeText.setText((String)input.readObject());
            connection.close();
            output.close();
            input.close();
         }
      }
      catch (HeadlessException e){
         e.printStackTrace();
      } 
      catch (ClassNotFoundException e){
         e.printStackTrace();
      }
       catch (UnknownHostException e){
         e.printStackTrace();
      } catch (IOException e){
         e.printStackTrace();
      }
   }
   
   private static void sendDate (Object obj){
      try {
         output.flush();
         output.writeObject(obj);
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
